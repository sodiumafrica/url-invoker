package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

var cron_url = "http://192.168.99.100:15001/api/hosts/refresh"
var log_path = "/var/www/cron.log" // Inside Go container

func main() {

	for { // Loop forever
		response, err := http.Get(cron_url)
		if err != nil {
			fmt.Printf("%s", err)
			logMessage(err, "ERROR")
			// os.Exit(1)
		} else {
			defer response.Body.Close()
			contents, err := ioutil.ReadAll(response.Body)
			if err != nil {
				fmt.Printf("%s", err)
				logMessage(err, "ERROR")
				// os.Exit(1)
			}
			fmt.Printf("%s\n", string(contents))
		}

		time.Sleep(3 * time.Second)
	}

}

func logMessage(msg error, prefix string) {
	f, err := os.OpenFile(log_path, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Println(err)
	}
	defer f.Close()

	logger := log.New(f, prefix+" => ", log.LstdFlags)
	logger.Println(" ", msg)
}
